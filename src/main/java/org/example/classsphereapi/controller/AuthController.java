package org.example.classsphereapi.controller;


import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import org.apache.ibatis.javassist.NotFoundException;
import org.example.classsphereapi.jwt.JwtService;
import org.example.classsphereapi.model.dto.request.AuthRequest;
import org.example.classsphereapi.model.dto.request.ForgetPasswordRequest;
import org.example.classsphereapi.model.dto.request.UserRequest;
import org.example.classsphereapi.model.dto.response.ApiResponse;
import org.example.classsphereapi.model.dto.response.AuthResponse;
import org.example.classsphereapi.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    private void authenticate(String username, String password) throws Exception {
        try {
            UserDetails userApp = authService.loadUserByUsername(username);
            if (userApp == null) {
                throw new NotFoundException("Wrong Email");
            }
            if (!passwordEncoder.matches(password, userApp.getPassword())) {
                throw new NotFoundException("Wrong Password");
            }
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@RequestBody @Valid AuthRequest authRequest) throws Exception {
        authenticate(authRequest.getEmail(), authRequest.getPassword());
        final UserDetails userDetails = authService.loadUserByUsername(authRequest.getEmail());
        final String token = jwtService.generateToken(userDetails);
        ApiResponse<?> response = ApiResponse.builder()
                .message("user login successfully")
                .payload(token)
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid UserRequest userRequest) {
        ApiResponse<?> response = ApiResponse.builder()
                .message("Register successfully")
                .payload(authService.register(userRequest))
                .status(HttpStatus.CONTINUE)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @PostMapping("/resend")
    public ResponseEntity<?> resendCode(String email) {
        authService.resendCode(email);
        return ResponseEntity.status(HttpStatus.OK).body("Code has been send.");
    }
    @PutMapping("/verify")
    public ResponseEntity<?> verifyOtpCode(String otpCode) {
        authService.verifyOtpCode(otpCode);
        return ResponseEntity.status(HttpStatus.OK).body("Code has been verified.");
    }
    @PutMapping("/forget")
    public ResponseEntity<?> forgetPassword(String email, @RequestBody @Valid ForgetPasswordRequest forgetPasswordRequest) {
        authService.forgetPassword(email, forgetPasswordRequest);
        return ResponseEntity.ok("Password was changed successful");
    }

}

