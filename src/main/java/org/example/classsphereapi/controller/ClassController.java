package org.example.classsphereapi.controller;


import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.example.classsphereapi.model.dto.request.ClassRequest;
import org.example.classsphereapi.model.dto.response.ApiResponse;
import org.example.classsphereapi.service.ClassService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;

import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/classes")
@SecurityRequirement(name = "bearerAuth")
public class ClassController {
    private final ClassService classService;

    @GetMapping("/getAllClass")
    public ResponseEntity<?> getAllClass(){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Get all classes successfully")
                .payload(classService.getAllClass())
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/getClassById/{classId}")
    public ResponseEntity<?> getClassById(@PathVariable String classId){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Get all classes successfully")
                .payload(classService.getClassById(classId))
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping("/createClass")
    public ResponseEntity<?> createClass(@RequestBody ClassRequest classRequest){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Class has been created")
                .payload(classService.createClass(classRequest))
                .status(HttpStatus.CREATED)
                .time(LocalDateTime.now())
                .build();
        System.out.println("Response: " + classRequest);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/updateClassById/{classId}")
    public ResponseEntity<?> updateClassById(@PathVariable String classId , @RequestBody ClassRequest classRequest){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Class has been updated")
                .payload(classService.updateClassById(classId,classRequest))
                .status(HttpStatus.CREATED)
                .time(LocalDateTime.now())
                .build();
        System.out.println("Response: " + classService.updateClassById(classId,classRequest));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/deleteClassById/{classId}")
    public ResponseEntity<?> deleteClassById(@PathVariable String classId){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Class has been deleted.")
                .payload(classService.deleteClassById(classId))
                .status(HttpStatus.CONTINUE)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/joinClassByCode/{classCode}")
    public ResponseEntity<?> joinClassByCode(@PathVariable String classCode){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Join class by code successfully.")
                .payload(classService.joinClassByCode(classCode))
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }
}
