package org.example.classsphereapi.controller;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.example.classsphereapi.model.dto.request.EventRequest;
import org.example.classsphereapi.model.dto.response.ApiResponse;
import org.example.classsphereapi.service.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;



@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/events")
@SecurityRequirement(name = "bearerAuth")
public class EventController {
    private final EventService eventService;

    @PostMapping("/createEvent")
    public ResponseEntity<?> createEvent(@RequestBody EventRequest eventRequest) {
        ApiResponse<?> response = ApiResponse.builder()
                .message("Event has been created successfully.")
                .payload(eventService.createEvent(eventRequest))
                .status(HttpStatus.CREATED)
                .time(LocalDateTime.now())
                .build();
        System.out.println(eventService.createEvent(eventRequest));
        System.out.println("Request Event : "+eventRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getAllEvent")
    public ResponseEntity<?> getAllEvent() {
        ApiResponse<?> response = ApiResponse.builder()
                .message("Get all events successfully")
                .payload(eventService.getAllEvent())
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getEventById/{eventId}")
    public ResponseEntity<?> getEventById(@PathVariable String eventId) {
        ApiResponse<?> response = ApiResponse.builder()
                .message("Get event by id successfully")
                .payload(eventService.getEventById(eventId))
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }



    @PutMapping("/updateEventById/{eventId}")
    public ResponseEntity<?> updateEventById(@PathVariable String eventId, @RequestBody EventRequest eventRequest){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Event has been updated.")
                .payload(eventService.updateEventById(eventId,eventRequest))
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/deleteEventById/{eventId}")
    public ResponseEntity<?> deleteEventById(@PathVariable String eventId){
        ApiResponse<?> response = ApiResponse.builder()
                .message("Event has been deleted.")
                .payload(eventService.deleteEventById(eventId))
                .status(HttpStatus.OK)
                .time(LocalDateTime.now())
                .build();
      //  System.out.println("Event : "+eventService.deleteEventById(eventId));
        return ResponseEntity.ok(response);
    }
}
