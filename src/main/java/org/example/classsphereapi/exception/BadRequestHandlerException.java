package org.example.classsphereapi.exception;

public class BadRequestHandlerException extends RuntimeException{
    public BadRequestHandlerException(String message) {
        super(message);
    }
}
