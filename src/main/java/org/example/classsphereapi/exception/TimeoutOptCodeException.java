package org.example.classsphereapi.exception;

public class TimeoutOptCodeException extends RuntimeException{
    public TimeoutOptCodeException(String message) {
        super(message);
    }
}
