package org.example.classsphereapi.model.dto.request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassRequest {
    private String className;
    private String description;
    private String profileUrl;
}
