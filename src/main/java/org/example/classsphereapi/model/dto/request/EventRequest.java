package org.example.classsphereapi.model.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventRequest {
    private String eventTitle;
    private String eventImage;
    private String eventDescription;
    private Boolean isDraft;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;
}
