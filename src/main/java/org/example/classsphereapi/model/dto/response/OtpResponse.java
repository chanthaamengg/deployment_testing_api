package org.example.classsphereapi.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtpResponse {
    private String otpCode;
    private LocalDateTime createdAt;
    private LocalDateTime expiredAt;
    private UUID userId;
}
