package org.example.classsphereapi.model.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private UUID userId;
    private String firstName;
    private String lastName;
    private String gender;
    private String phone;
    private String email;
    @JsonIgnore
    private String password;
    private LocalDateTime dob;
    private boolean isVerify;
    private String profileUrl;
}