package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Announcements {
    private UUID announceId;
    private String title;
    private String description;
    private String imageUrl;
    private LocalDateTime createdAt;
    private Boolean isDraft;
}
