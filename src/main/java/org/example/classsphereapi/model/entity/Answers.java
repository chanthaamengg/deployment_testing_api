package org.example.classsphereapi.model.entity;
import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answers {
    private UUID answerId;
    private JSONObject answerForm;
    private Boolean isCheck;
    private LocalDateTime createdAt;
}
