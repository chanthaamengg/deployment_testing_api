package org.example.classsphereapi.model.entity;


import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassWorks {
    private UUID classworkId;
    private String classworkTitle;
    private String subject;
    private String instruction;
    private JSONObject form;
    private Boolean isDraft;
    private Boolean isExam;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;
}
