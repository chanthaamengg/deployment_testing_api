package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classes {
    private UUID classId;
    private String className;
    private String description;
    private String profileUrl;
    private String classCode;
    private LocalDateTime createdAt;
}
