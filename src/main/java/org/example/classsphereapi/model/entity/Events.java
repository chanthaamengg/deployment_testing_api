package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Events {
    private UUID eventId;
    private String eventTitle;
    private String eventImage;
    private String eventDescription;
    private LocalDateTime createdAt;
    private Boolean isDraft;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;
    private UUID userId;
    private UUID classId;
}
