package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class LinkAttachments {
    private UUID linkId;
    private String linkTitle;
    private String link;
    private LocalDateTime uploadedAt;
}
