package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Materials {
    private UUID materialId;
    private String materialTitle;
    private String description;
    private LocalDateTime createdAt;
}
