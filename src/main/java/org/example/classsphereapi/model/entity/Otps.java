package org.example.classsphereapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Otps {
    private UUID otpId;
    private String otpCode;
    private Boolean isVerify;
    private LocalDateTime createdAt;
    private LocalDateTime expiredAt;
    private UUID userId;
}
