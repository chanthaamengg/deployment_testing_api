package org.example.classsphereapi.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subjects {
    private UUID subjectId;
    private String subjectName;
    private String description;
    private String imageUrl;
    private LocalDateTime createdAt;
}
