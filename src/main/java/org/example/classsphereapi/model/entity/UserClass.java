package org.example.classsphereapi.model.entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserClass {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID id;
    private Boolean isTeacher;
    private Boolean isDisabled;
    private UUID classId;
    private UUID userId;
}
