package org.example.classsphereapi.repository;

import org.apache.ibatis.annotations.*;
import org.example.classsphereapi.config.UUIDTypeHandler;
import org.example.classsphereapi.model.dto.request.UserRequest;
import org.example.classsphereapi.model.dto.response.UserResponse;

@Mapper
public interface AuthRepository {
    @Results(id = "userMapper",value = {
            @Result(property = "userId",column = "user_id",typeHandler = UUIDTypeHandler.class ),
            @Result(property = "firstName",column = "first_name"),
            @Result(property = "lastName",column = "last_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "telephone", column = "phone"),
            @Result(property = "email" , column = "email"),
            @Result(property = "verify", column = "is_verify")
    })
    @Select("insert into users (first_name,last_name,email,password) values (#{req.firstName},#{req.lastName},#{req.email},#{req.password}) returning *")
    UserResponse insertUserInfo(@Param("req") UserRequest authRequest);

    @ResultMap("userMapper")
    @Select("select * from users where email=#{email}")
    UserResponse getUserByEmail(String email);

    @ResultMap("userMapper")
    @Update(" UPDATE users SET password = #{confirmPassword} WHERE email = #{email}")
    void updatePassword(String confirmPassword,String email);


    @Insert("Insert into users (profile_url) values (#{profile})")
    boolean insertProfile(String profile);

    @Update( "UPDATE users SET is_verify = #{value} WHERE user_id = CAST(#{userId} AS UUID)")
    void updateStatus(boolean value, String userId);

    @ResultMap("userMapper")
    @Select("""
 UPDATE users   SET first_name = #{user.firstName},
                  last_name = #{user.lastName},
                  password = #{user.password}
  WHERE email = #{email} returning *
""")
    UserResponse updateUserInfoByEmail(@Param("user") UserRequest userRequest, String email);
}
