package org.example.classsphereapi.repository;
import org.apache.ibatis.annotations.*;
import org.example.classsphereapi.config.UUIDTypeHandler;
import org.example.classsphereapi.model.dto.request.ClassRequest;
import org.example.classsphereapi.model.entity.Classes;


import java.util.List;
import java.util.UUID;

@Mapper
public interface ClassRepository {

    @Results(id = "classMapper",value = {
            @Result(property = "classId",column = "class_id",typeHandler = UUIDTypeHandler.class,javaType = UUID.class),
            @Result(property = "className",column = "class_name"),
            @Result(property = "profileUrl",column = "profile_url"),
            @Result(property = "classCode",column = "class_code"),
            @Result(property = "createdAt",column = "created_at"),
    })


    //insert
    @Select("insert into classes (class_name,description,profile_url,class_code) values (#{class.className},#{class.description},#{class.profileUrl},#{classCode}) returning *")
    Classes createClass(@Param("class") ClassRequest classRequest , String classCode);

    //get all class
    @Select("select * from classes inner join user_classes on classes.class_id = user_classes.class_id where user_id=CAST(#{userId} as uuid) ")
    @ResultMap("classMapper")
    List<Classes> getAllClass(String userId);

    // get class by id
    @Select("select * from classes inner join user_classes on classes.class_id = user_classes.class_id  where class_id = CAST(#{classId} AS uuid)")
    @ResultMap("classMapper")
    Classes getClassById(String classId);

    //update
    @Select("update classes set class_name=#{class.className},description=#{class.description},profile_url=#{class.profileUrl} where class_id=CAST(#{classId} as uuid)returning *")
    @ResultMap("classMapper")
    Classes updateClassById(String classId, @Param("class") ClassRequest classRequest);

    //delete
    @Delete("delete from classes where class_id=CAST(#{classId} as uuid)")
    Boolean deleteClassById(String classId);

    // join class
    @Select("select * from classes where class_code = #{classCode}")
    @ResultMap("classMapper")
    Classes joinClassByCode(String classCode);
}
