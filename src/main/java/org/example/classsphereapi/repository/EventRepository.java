package org.example.classsphereapi.repository;
import org.apache.ibatis.annotations.*;
import org.example.classsphereapi.config.UUIDTypeHandler;
import org.example.classsphereapi.model.dto.request.EventRequest;
import org.example.classsphereapi.model.entity.Events;
import org.springframework.security.core.parameters.P;

import java.util.List;
import java.util.UUID;


@Mapper
public interface EventRepository {
    @Results(id = "eventMapper",value = {
            @Result(property = "eventId",column = "event_id",typeHandler = UUIDTypeHandler.class,javaType = UUID.class),
            @Result(property = "eventTitle",column = "event_title"),
            @Result(property = "eventImage",column = "event_image"),
            @Result(property = "eventDescription",column = "event_description"),
            @Result(property = "createdAt",column = "created_at"),
            @Result(property = "isDraft",column = "is_draft"),
            @Result(property = "startedAt",column = "started_at"),
            @Result(property = "endedAt",column = "ended_at"),
            @Result(property = "userId",column = "user_id",typeHandler = UUIDTypeHandler.class),
            @Result(property = "classId",column = "class_id",typeHandler = UUIDTypeHandler.class),
          // @Result(property = "classId",column = "class_id",typeHandler = UUIDTypeHandler.class,one = @One(select = "org.example.classsphereapi.repository.ClassRepository.getClassById")),
    })

    @Select("insert into events(event_title,event_image,event_description,is_draft,started_at,ended_at) values (#{event.eventTitle},#{event.eventImage},#{event.eventDescription},#{event.isDraft},#{event.startedAt},#{event.endedAt}) returning *")
    Events createEvent(@Param("event") EventRequest eventRequest);


    @Select("select * from events")
    @ResultMap("eventMapper")
    List<Events> getAllEvent();

    @Select("update events set event_title=#{event.eventTitle},event_image=#{event.eventImage},event_description=#{event.eventDescription},is_draft=#{event.isDraft} where event_id = CAST(#{eventId} as uuid) returning *")
    @ResultMap("eventMapper")
    Events updateEventById(String eventId,@Param("event") EventRequest eventRequest);

    @Select("delete from events where event_id=CAST(#{id} as uuid)")
    @ResultMap("eventMapper")
    Boolean deleteEventById(String eventId);


    @Select("select * from events where event_id= CAST(#{eventId} as uuid)")
    @ResultMap("eventMapper")
    Events getEventById(String eventId);
}
