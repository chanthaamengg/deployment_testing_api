package org.example.classsphereapi.repository;
import org.apache.ibatis.annotations.*;
import org.example.classsphereapi.config.UUIDTypeHandler;
import org.example.classsphereapi.model.dto.response.OtpResponse;
import java.time.LocalDateTime;
import java.util.UUID;

@Mapper
public interface OtpRepository {

    @Select("insert into otps(otp_code,is_verify,created_at,expired_at,user_id) values (#{code},#{isVerify},#{createdAt},#{expiredAt},CAST(#{userId} as uuid))")
    @Result(property = "otpCode",column = "otp_code")
    @Result(property = "createdAt", column = "created_at")
    @Result(property = "expiredAt", column = "expired_at")
    @Result(property = "userId", column = "user_id" ,javaType = UUID.class,typeHandler = UUIDTypeHandler.class)
    void insertOtp(String code,boolean isVerify,LocalDateTime createdAt,LocalDateTime expiredAt, String userId);

    @Select("SELECT * FROM otps WHERE otp_code=#{otpCode}")
    @Result(property = "otpCode",column = "otp_code")
    @Result(property = "createdAt", column = "created_at")
    @Result(property = "expiredAt", column = "expired_at")
    @Result(property = "userId", column = "user_id" ,javaType = UUID.class,typeHandler = UUIDTypeHandler.class)
    OtpResponse getOtpByOtpCode(String otpCode);
}
