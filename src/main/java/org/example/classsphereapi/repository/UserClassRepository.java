package org.example.classsphereapi.repository;

import org.apache.ibatis.annotations.*;
import org.example.classsphereapi.model.entity.Classes;

import java.util.UUID;

@Mapper
public interface UserClassRepository {

    @Insert("""
insert into user_classes (is_teacher,class_id,user_id) values (
 #{isTeacher},CAST(#{classId} AS UUID),CAST(#{userId} AS UUID)
)
    """)
    void insertRole(boolean isTeacher, String classId, String userId);
}
