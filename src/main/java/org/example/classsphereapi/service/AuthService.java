package org.example.classsphereapi.service;

import org.example.classsphereapi.model.dto.request.ForgetPasswordRequest;
import org.example.classsphereapi.model.dto.request.UserRequest;
import org.example.classsphereapi.model.dto.response.UserResponse;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface AuthService extends UserDetailsService {
    UserResponse register(UserRequest userRequest);

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    void resendCode(String email);

    void verifyOtpCode(String otpCode);

    void forgetPassword(String email, ForgetPasswordRequest forgetPasswordRequest);

}
