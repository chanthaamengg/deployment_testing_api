package org.example.classsphereapi.service;

import org.example.classsphereapi.model.dto.request.ClassRequest;
import org.example.classsphereapi.model.entity.Classes;

import java.util.List;
import java.util.UUID;

public interface ClassService {
    Classes createClass(ClassRequest classRequest);

    List<Classes> getAllClass();

    Classes updateClassById(String classId, ClassRequest classRequest);

    Boolean deleteClassById(String classId);

    Classes getClassById(String classId);

    Classes joinClassByCode(String classCode);
}
