package org.example.classsphereapi.service;
import jakarta.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface EmailService {
    void sendEmail(String email, String otp) throws MessagingException, UnsupportedEncodingException;
}
