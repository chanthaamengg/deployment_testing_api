package org.example.classsphereapi.service;
import org.example.classsphereapi.model.dto.request.EventRequest;
import org.example.classsphereapi.model.entity.Events;
import java.util.List;
import java.util.UUID;


public interface EventService {
    Events createEvent(EventRequest eventRequest);

    List<Events> getAllEvent();

    Events updateEventById(String eventId, EventRequest eventRequest);

    Boolean deleteEventById(String eventId);

    Events getEventById(String eventId);
}
