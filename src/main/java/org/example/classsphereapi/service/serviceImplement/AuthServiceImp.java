package org.example.classsphereapi.service.serviceImplement;

import jakarta.mail.MessagingException;
import lombok.AllArgsConstructor;
import org.example.classsphereapi.exception.*;
import org.example.classsphereapi.model.dto.response.OtpResponse;
import org.example.classsphereapi.model.entity.Users;
import org.example.classsphereapi.model.dto.request.ForgetPasswordRequest;
import org.example.classsphereapi.model.dto.request.UserRequest;
import org.example.classsphereapi.model.dto.response.UserResponse;
import org.example.classsphereapi.repository.AuthRepository;
import org.example.classsphereapi.repository.OtpRepository;
import org.example.classsphereapi.service.AuthService;
import org.example.classsphereapi.service.EmailService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.Random;

@Service
@AllArgsConstructor
public class AuthServiceImp implements AuthService {
    private final AuthRepository authRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final EmailService emailService;
    private final OtpRepository otpRepository;
    private final ModelMapper modelMapper;
    @Override
    public UserResponse register(UserRequest userRequest) {
        UserResponse response =authRepository.getUserByEmail(userRequest.getEmail());
        if (!userRequest.getPassword().equals(userRequest.getConfirmPassword())) {
            throw new NotAllowedException("Password not match");
        }
        userRequest.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        if(response==null){
           response = authRepository.insertUserInfo(userRequest);
        }else if (!response.isVerify()){
            response = authRepository.updateUserInfoByEmail(userRequest,userRequest.getEmail());
        }else {
            throw new BadRequestHandlerException("Email was already exist");
        }
        Random random = new Random();
        String optCode = String.valueOf( random.nextInt(8999)+1000);
            try {
                emailService.sendEmail(userRequest.getEmail(), optCode);
                otpRepository.insertOtp(optCode,false, LocalDateTime.now(),LocalDateTime.now().plusMinutes(2),String.valueOf(response.getUserId()));
            } catch (MessagingException | UnsupportedEncodingException  e) {
                System.out.println(e.getMessage());
            }
        return response ;
    }
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return modelMapper.map(authRepository.getUserByEmail(email),Users.class);
    }
    @Override
    public void resendCode(String email) {
        Random random = new Random();
        String otpCode = String.valueOf(random.nextInt(8999)+1000);
        try {
            emailService.sendEmail(email,otpCode);
            System.out.println("Code: "+otpCode);
            System.out.println(email);
            otpRepository.insertOtp(otpCode,
                    false,
                    LocalDateTime.now(),
                    LocalDateTime.now().plusMinutes(2),
                    String.valueOf(authRepository.getUserByEmail(email).getUserId()));
        } catch (MessagingException | UnsupportedEncodingException  e) {
            System.out.println(e.getMessage());
        }
    }
    @Override
    public void verifyOtpCode(String otpCode) {
        OtpResponse otp = otpRepository.getOtpByOtpCode(otpCode);
        if(otp==null||!otp.getOtpCode().equals(otpCode)){
            throw new NotFoundException("Invalid otp code");
        } else if (otp.getExpiredAt().isBefore(LocalDateTime.now())) {
            throw new TimeoutOptCodeException("Otp code already expired");
        }else if(otp.getOtpCode().equals(otpCode)) {
            System.out.println("userId"+otp.getUserId());
            authRepository.updateStatus(true,String.valueOf(otp.getUserId()));
        }
    }
    @Override
    public void forgetPassword(String email, ForgetPasswordRequest forgetPasswordRequest) {
        UserResponse userResponse = authRepository.getUserByEmail(email);
        if(userResponse==null || !userResponse.isVerify()) {
            throw new NotFoundException("Email Not found ");
        }
        else if(!forgetPasswordRequest.getPassword().equals(forgetPasswordRequest.getConfirmPassword())){
            throw new NoContentException("Confirm Password doesn't match to Curren Password");
        }else{

            System.out.println("new password: "+bCryptPasswordEncoder.encode(forgetPasswordRequest.getConfirmPassword()));
            authRepository.updatePassword(bCryptPasswordEncoder.encode(forgetPasswordRequest.getConfirmPassword()),userResponse.getEmail());
        }
    }
}
