package org.example.classsphereapi.service.serviceImplement;

import lombok.AllArgsConstructor;
import org.example.classsphereapi.exception.NotAllowedException;
import org.example.classsphereapi.model.dto.response.UserResponse;
import org.example.classsphereapi.exception.NotFoundException;
import org.example.classsphereapi.model.dto.request.ClassRequest;

import org.example.classsphereapi.model.entity.Classes;
import org.example.classsphereapi.model.entity.Users;
import org.example.classsphereapi.repository.AuthRepository;
import org.example.classsphereapi.repository.ClassRepository;
import org.example.classsphereapi.repository.UserClassRepository;
import org.example.classsphereapi.service.ClassService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class ClassServiceImp implements ClassService {
    private final ClassRepository classRepository;
    private final AuthRepository authRepository;
    private final UserClassRepository userClassRepository;

    //generate class code
    public String getRandomString(int size) {
        String allCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder sb = new StringBuilder(size);
        int length = allCharacters.length();
        for (int i = 0; i < size; i++) {
            sb.append(allCharacters.charAt((int) (length * Math.random())));
        }
        return sb.toString();
    }

    String getUsernameOfCurrentUser() {
        Users userDetails = (Users) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return userDetails.getUsername();
    }

    @Override
    public Classes createClass(ClassRequest classRequest) {
        String currentUser = getUsernameOfCurrentUser();
        UserResponse userResponse = authRepository.getUserByEmail(currentUser);
        Classes classes = classRepository.createClass(classRequest, getRandomString(8));
        userClassRepository.insertRole(true, String.valueOf(classes.getClassId()), String.valueOf(userResponse.getUserId()));
        return classes;
    }

    @Override
    public List<Classes> getAllClass() {
        String currentUser = getUsernameOfCurrentUser();
        UserResponse userResponse = authRepository.getUserByEmail(currentUser);
        return classRepository.getAllClass(String.valueOf(userResponse.getUserId()));
    }

    @Override
    public Classes updateClassById(String classId, ClassRequest classRequest) {
        String currentUser = getUsernameOfCurrentUser();
        UserResponse userResponse = authRepository.getUserByEmail(currentUser);
        Classes classes = classRepository.updateClassById(classId, classRequest);
        if(classes == null) {
            throw new NotFoundException("Class id " + classId + " not found");
        }
        return classRepository.updateClassById(classId, classRequest);
    }

    @Override
    public Boolean deleteClassById(String classId) {
        return classRepository.deleteClassById(classId);
    }

    @Override
    public Classes getClassById(String classId) {
       Classes classes = classRepository.getClassById(classId);
       if(classes == null) {
           throw new NotFoundException("Class id " + classId + " not found");
       }
        return classRepository.getClassById(classId);
    }

    @Override
    public Classes joinClassByCode(String classCode) {
        String currentUser = getUsernameOfCurrentUser();
        UserResponse userResponse = authRepository.getUserByEmail(currentUser);
        String[] codePart = classCode.split("-",2);
        boolean isTeacher;
        if (codePart[0].equals("stu")){
            isTeacher=false;
        } else if (codePart[0].equals("cher")) {
            isTeacher=true;
        }else {
            throw new NotAllowedException("Invalid Code");
        }
        Classes classes = classRepository.joinClassByCode(codePart[1]);
        if (classes.getClassCode().equals(codePart[1])) {
            userClassRepository.insertRole(isTeacher, String.valueOf(classes.getClassId()), String.valueOf(userResponse.getUserId()));
        }
        return classes;
    }
}
