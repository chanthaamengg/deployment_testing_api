package org.example.classsphereapi.service.serviceImplement;

import lombok.AllArgsConstructor;
import org.example.classsphereapi.model.dto.request.EventRequest;
import org.example.classsphereapi.model.entity.Events;
import org.example.classsphereapi.repository.EventRepository;
import org.example.classsphereapi.service.EventService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class EventServiceImp implements EventService {
    private final EventRepository eventRepository;
//    private final UserClassRepository userClassRepository;
//    private final ClassRepository classRepository;

    @Override
    public Events createEvent(EventRequest eventRequest) {
        return eventRepository.createEvent(eventRequest);
    }

    @Override
    public List<Events> getAllEvent() {
        return eventRepository.getAllEvent();
    }

    @Override
    public Events updateEventById(String eventId, EventRequest eventRequest) {
        return eventRepository.updateEventById(eventId,eventRequest);
    }

    @Override
    public Boolean deleteEventById(String eventId) {
        return eventRepository.deleteEventById(eventId);
    }

    @Override
    public Events getEventById(String eventId) {
        return eventRepository.getEventById(eventId);
    }
}
