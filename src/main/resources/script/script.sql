-- Active: 1715068857542@@110.74.194.124@5435@cognito_db@public
-- Active: 1707985030528@@127.0.0.1@5432@class_sphere@public

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE users (
                       user_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                       first_name VARCHAR(32) NOT NULL,
                       last_name VARCHAR(32) NOT NULL,
                       gender VARCHAR(8) ,
                       phone VARCHAR (15),
                       email VARCHAR (64) NOT NULL,
                       password VARCHAR (32) NOT NULL
);

CREATE TABLE otps (
                      otp_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                      code VARCHAR(6) NOT NULL,
                      verify BOOLEAN DEFAULT false,
                      created_at TIMESTAMP NOT NULL,
                      expired_at TIMESTAMP NOT NULL,
                      user_id UUID ,
                      CONSTRAINT fk_userID_otps FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE classes (
                         class_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                         class_name VARCHAR(64) NOT NULL,
                         description TEXT,
                         profile_url VARCHAR(255),
                         class_code VARCHAR(10) NOT NULL,
                         created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE user_classes (
                              id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                              is_teacher BOOLEAN DEFAULT false ,
                              is_disable BOOLEAN DEFAULT false ,
                              class_id UUID,
                              user_id UUID,
                              CONSTRAINT fk_classID_userClass FOREIGN KEY (class_id) REFERENCES classes (class_id) ON UPDATE CASCADE,
                              CONSTRAINT fk_userID_userClass FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE events (
                        event_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                        event_title VARCHAR(64) NOT NULL,
                        event_image VARCHAR(255),
                        evnet_description TEXT,
                        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        is_draft BOOLEAN DEFAULT false,
                        user_id UUID,
                        class_id UUID,
                        CONSTRAINT fk_classID_events FOREIGN KEY (class_id) REFERENCES classes (class_id) ON UPDATE CASCADE,
                        CONSTRAINT fk_userID_events FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE announcements (
                               ann_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                               title  VARCHAR (255) NOT NULL,
                               ann_description TEXT,
                               ann_image VARCHAR(255),
                               created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                               is_draft BOOLEAN ,
                               user_id UUID,
                               CONSTRAINT fk_userID_announcements FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE link_attachments(
                                 link_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                                 link_title VARCHAR (255) NOT NULL,
                                 link_url VARCHAR (255) NOT NULL,
                                 uploaded_at TIMESTAMP ,
                                 ann_id UUID ,
                                 CONSTRAINT fk_annID_link_attachments FOREIGN KEY  (ann_id) REFERENCES announcements (ann_id) ON UPDATE CASCADE
);

CREATE TABLE announcement_comments(
                                      ann_comment_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                                      content TEXT ,
                                      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
                                      user_id UUID,
                                      ann_id UUID,
                                      CONSTRAINT fk_userID_announcement_comments FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE,
                                      CONSTRAINT fk_annID_announcement_comments FOREIGN KEY (ann_id) REFERENCES announcements (ann_id) ON UPDATE CASCADE
);

CREATE TABLE subjects(
                         sub_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                         sub_name VARCHAR(64) NOT NULL,
                         sub_description TEXT ,
                         image_url VARCHAR(255),
                         created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
                         created_by UUID ,
                         CONSTRAINT fk_createdBy_subjects FOREIGN KEY (created_by) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE class_subjects (
                                id UUID  DEFAULT uuid_generate_v4() PRIMARY KEY,
                                class_id UUID,
                                sub_id UUID,
                                CONSTRAINT fk_classID_class_subjects FOREIGN KEY (class_id) REFERENCES classes (class_id) ON UPDATE CASCADE,
                                CONSTRAINT fk_subID_class_subjects FOREIGN KEY (sub_id) REFERENCES subjects (sub_id) ON UPDATE CASCADE
);

CREATE TABLE materials (
                           mat_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                           mat_title VARCHAR (64) NOT NULL,
                           mat_description  TEXT ,
                           image_url VARCHAR(255) ,
                           created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                           sub_id UUID,
                           created_by UUID,
                           CONSTRAINT fk_subID_class_materials FOREIGN KEY (sub_id) REFERENCES subjects (sub_id) ON UPDATE CASCADE,
                           CONSTRAINT fk_userID_materials FOREIGN KEY (created_by) REFERENCES users (user_id) ON UPDATE CASCADE
);

CREATE TABLE material_comments (
                                   mat_com_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                                   content VARCHAR (255) NOT NULL ,
                                   created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
                                   mat_id UUID,
                                   user_id UUID,
                                   CONSTRAINT fk_userID_material_comments FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE,
                                   CONSTRAINT fk_materialID_material_comments FOREIGN KEY (mat_id) REFERENCES materials (mat_id) ON UPDATE CASCADE
);

CREATE TABLE material_links(
                               mat_link_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY ,
                               title VARCHAR(64),
                               file_url VARCHAR(255) NOT NULL,
                               is_downloaded_link BOOLEAN DEFAULT false,
                               created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                               mat_id UUID,
                               CONSTRAINT fk_materialID_material_links FOREIGN KEY (mat_id) REFERENCES materials (mat_id) ON UPDATE CASCADE
);